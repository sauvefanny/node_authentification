
## How to Use
1. Créer une base de données et importer le fichier dedans (exemple: `mysql -u simplon -p node_authentication < db.sql`)
2. `npm i`
3. Créer un fichier .env avec une DATABASE_URL pour la database créée
4. Générer une paire de clef privée/clef publique dans un dossier config (qu'il faut créer)
```
ssh-keygen -t rsa -P "" -b 4096 -m PEM -f config/id_rsa
ssh-keygen -e -m PEM -f config/id_rsa > config/id_rsa.pub
```
5. Si on veut vérifier si ça marche : `npm tests`
6. `npm start` 
